


var timer = {
	minutesUpper : 0,
	timerPaused : false,
	init : el => {
		timer.outputEl = document.getElementById('output');
		timer.minutesInput = document.getElementById('minutes');
		timer.startButton = document.getElementById('start');
		timer.stopButton = document.getElementById('stop');
	},
	change : el => {
		timer.minutesUpper = el.value;
	},
	start : el => {
		timer.startButton.setAttribute('disabled', 'disabled');
		timer.stopButton.removeAttribute('disabled');
		
		timer.countdown();
	},
	stop : el => {
		clearInterval(timer.countdownInterval);
		if (timer.timerPaused) {
			timer.reset();
		}
		timer.timerPaused = true;
	},
	countdown : el => {
		timer.secondsLeft = Math.floor(timer.minutesUpper*60);
		
		timer.countdownInterval = setInterval(function() {

			timer.secondsLeft = timer.secondsLeft - 1;

			var minutes = Math.floor(timer.secondsLeft / 60);
			var seconds = timer.secondsLeft - minutes * 60;
			
			timer.outputEl.innerText = minutes + ':' + seconds;

			// If the count down is finished, write some text
			if (timer.secondsLeft < 1) {
				clearInterval(timer.countdownInterval);
				timer.reset();
			}
		}, 1000);
	},
	reset : () => {
		timer.stopButton.setAttribute('disabled', 'disabled');
		timer.startButton.removeAttribute('disabled');
		timer.outputEl.innerText = '';
		timer.minutesInput.value = '';
	}
}; 


  
  