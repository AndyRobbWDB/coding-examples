export const store = {
  debug: true,
  state: {
    text: 'Text from App.vue!'
  },
  setTextAction (newValue) {
    if (this.debug) console.log('setTextAction triggered with', newValue)
    this.state.text = newValue
  },
  clearTextAction () {
    if (this.debug) console.log('clearTextAction triggered')
    this.state.text = ''
  }
}