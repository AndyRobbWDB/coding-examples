# Simple carousel with the use of Slot

Complete the component Carousel.vue and create two instances of the carousel in App.vue. The carousel should display one card at a time. The carousel should contain two buttons, one to show the previous card and the other one to show the next card.

The RedCard and YellowCard components are given.

The first carousel has 7 cards, the card should be red when the index is an odd number and the card should be yellow when the index is even.

The second carousel has 6 cards, the card should be yellow when the index is an odd number and the card should be red when the index is even.

For every card, pass in the index for the text prop.
