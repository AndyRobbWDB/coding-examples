 new Vue({
	el: '#app',
	data: {
		num: 0,
	},
	methods: {
		decrease: function(e){
			this.num = (10===this.num) ? 0 : this.num + 1;
		}
	}
});