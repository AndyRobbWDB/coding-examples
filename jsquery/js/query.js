
var queryEl = function(targetID) {
  
	let targetEl = document.getElementById(targetID);	
	let headingEls = targetEl.getElementsByTagName('h1');	
	let paragraphEls = targetEl.getElementsByTagName('p');	
	let output = []

	if (headingEls && 0 < headingEls.length) {
		output.push({
			'title': headingEls[0].innerHTML
		});
	}

	if (paragraphEls && 0 < paragraphEls.length) {
		output.push({
			'content': paragraphEls[0].innerHTML
		});
	}
	return output;

}; 


  
  